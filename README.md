# Fedran Meta

This project contains most of the meta information for working with [Fedran](https://fedran.com/), mostly elements dealing with the process of writing including style guides and identifying characters and volumes.

-   [Style Guide](./style-guide.md) contains information about writing conventions, stylistic choices, and some language specifics. This is suitable for sending to editors along with the works.

## Inspiration

The following pages were helpful in creating these resources:

-   [Book Trigger Warnings](https://booktriggerwarnings.com/index.php?title=Book_Trigger_Warnings:List_of_Trigger_Warnings)
